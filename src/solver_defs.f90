!--------------------------------------------------------------------------------!
! See the LICENSE file for license information. Please report all bugs and       !
! problems to abhishekd18 at gmail.com                                           !
!--------------------------------------------------------------------------------!

module solver_defs
  use precision
  use math
  use rk_defs

  implicit none

  ! Newton-Raphson solver
  type nr_solver_t
     real(WP) :: relax_coeff, alpha, tol
     integer :: max_count
  end type nr_solver_t

  type solver_t

     ! Scheme
     character(len=32) :: scheme = 'LF'

     ! Order
     integer :: order = 1

     ! RK Solver
     type(rktvd_solver_t), pointer :: rktvd
     
     ! Convective scheme
     real(WP), dimension(:,:), pointer :: divc, weno3p, weno3m, weno5p, weno5m

     ! Optimal weights for weno3/5
     real(WP) :: a0, a1, a2

     ! Stencils for weno3/5
     real(WP), dimension(:), pointer :: S0_p, S1_p, S2_p, S0_m, S1_m, &
                                        S2_m, f_p, f_m, f_t

     ! Number of spray equations
     integer :: neq = 14

     ! Number of additional scalars
     integer :: nsc = 0
 
     ! State and flux vectors
     real(WP), dimension(:,:), pointer :: W, Wold, F, S, Res

     ! Flux, Residual, Wave speeds(liquid and gas phase)
     real(WP), dimension(:), pointer :: Flux, alpha_l, alpha_g, alpha_m

     ! Newton-Raphson solver
     type(nr_solver_t), pointer :: nr

     ! Strang splitting
     logical :: strang = .false.

  end type solver_t

contains

  subroutine allocate_solver(solver,nzo)
    implicit none
    
    ! ---------------------------------
    type(solver_t), pointer, intent(inout) :: solver
    integer, intent(in) :: nzo
    ! ---------------------------------

!!$    nullify(solver%rk, &
!!$            solver%divc, &
!!$            solver%weno5p, solver%weno5m, solver%S0_p, solver%S1_p,
!solver%S2_p, &
!!$            solver%S0_m, solver%S1_m, solver%S2_m, solver%f_p,
!solver%f_m, solver%f_t, &
!!$            solver%W, solver%Wold, solver%F, solver%S, solver%Res, &
!!$            solver%Flux, solver%alpha_l, solver%alpha_g)

    allocate(solver%W(solver%neq+solver%nsc,nzo)); solver%W = 0.0_WP
    allocate(solver%Wold(solver%neq+solver%nsc,nzo)); solver%Wold = 0.0_WP
    allocate(solver%F(solver%neq+solver%nsc,nzo)); solver%F = 0.0_WP
    allocate(solver%S(solver%neq+solver%nsc,nzo)); solver%S = 0.0_WP
    allocate(solver%Res(solver%neq+solver%nsc,nzo)); solver%Res = 0.0_WP

    allocate(solver%Flux(nzo)); solver%Flux = 0.0_WP
    allocate(solver%alpha_l(nzo)); solver%alpha_l = 0.0_WP
    allocate(solver%alpha_g(nzo)); solver%alpha_g = 0.0_WP
    allocate(solver%alpha_m(nzo)); solver%alpha_m = 0.0_WP

  end subroutine allocate_solver

  subroutine deallocate_solver(solver)
    implicit none
    
    ! ---------------------------------
    type(solver_t), pointer, intent(inout) :: solver

    ! ---------------------------------

    call deallocate_rktvd(solver%rktvd)

    deallocate(solver%divc)

    if(associated(solver%weno5p)) then
       deallocate(solver%weno5p, solver%weno5m, &
                  solver%S0_p, solver%S1_p, solver%S2_p, &
                  solver%S0_m, solver%S1_m, solver%S2_m, &
                  solver%f_p, solver%f_m, solver%f_t)
    end if

    if(associated(solver%weno3p)) then
       deallocate(solver%weno3p, solver%weno3m, &
                  solver%S0_p, solver%S1_p, &
                  solver%S0_m, solver%S1_m, &
                  solver%f_p, solver%f_m, solver%f_t)
    end if

    deallocate(solver%W, solver%Wold, solver%F, solver%S)
    
    deallocate(solver%Flux, solver%Res, solver%alpha_l, solver%alpha_g, solver%alpha_m)
    
    deallocate(solver%nr)

    deallocate(solver)

  end subroutine deallocate_solver

  !subroutine assign_SolverVec(SolverVecA,SolverVecB,spray)
    
  !end subroutine assign_SolverVec

end module solver_defs

